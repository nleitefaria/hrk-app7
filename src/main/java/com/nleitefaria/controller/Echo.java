package com.nleitefaria.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class Echo {
	
	@GetMapping("/")
    public String index(final Model model) {
        model.addAttribute("title", "Docker + Spring Boot");
        model.addAttribute("msg", "Welcome to the docker container!");
        return "index";
    }
	
	@GetMapping("/hi")
    public String hi(final Model model) {
        model.addAttribute("title", "Hi");
        model.addAttribute("msg", "Hello!");
        model.addAttribute("title", "Docker + Spring Boot");
        model.addAttribute("msg", "hi!");
        return "index";
    }

}
